package com.rnm.test.starwarscharacters;




import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import com.rnm.test.starwarscharacters.data.StarWarsCharacters;
import com.rnm.test.starwarscharacters.data.StarWarsPersonsDetails;
import com.rnm.test.starwarscharacters.data.StarWarsResults;
import com.rnm.test.starwarscharacters.utilities.APIEndPoints;
import com.rnm.test.starwarscharacters.utilities.RetrofitBuilder;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity  {


    private static String TAG = MainActivity.class.getSimpleName();


    private RecyclerView recyclerView;
    private StarWarsAdapter adapter;
    private List<StarWarsPersonsDetails> detailsList;
    private CompositeDisposable _disposables;
    private FloatingActionButton fab_left, fab_right;
    private APIEndPoints service;
    private int num;
    private static final int COUNT= 48;
    private String next, previous;
    private ContentLoadingProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        num =1;
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        fab_left = (FloatingActionButton) findViewById(R.id.fab_left);
        fab_right = (FloatingActionButton) findViewById(R.id.fab_right);
        progressBar = (ContentLoadingProgressBar) findViewById(R.id.progress_bar);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressBar.show();

        _disposables = new CompositeDisposable();

        detailsList = new ArrayList<>();
        service = new RetrofitBuilder().getStarWarsDetails();
        startWarsCharList();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                {
                    fab_right.show();
                    if(num >1){
                        fab_left.show();
                    }
                }
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0 ||dy<0 && fab_right.isShown()  )
                {
                        fab_right.hide();

                    if(num <=1 ){
                        fab_left.hide();
                    }
                }
            }
        });
        fab_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                num = next.charAt(34) - COUNT;
                startWarsCharList();
            }
        });
        fab_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                num = previous.charAt(34) - COUNT;
                startWarsCharList();
            }
        });




    }

    public void startWarsCharList() {

       _disposables.add(service
               .getStarWarsCharacters(num)
               .subscribeOn(Schedulers.io())
               .observeOn(AndroidSchedulers.mainThread())
               .subscribeWith(
                       new DisposableObserver<StarWarsResults>() {

                           @Override
                           public void onNext(@NonNull StarWarsResults starWarsResults) {
                               Log.d(TAG,""+starWarsResults);
                               getStarWarsCharList(starWarsResults);

                           }

                           @Override
                           public void onError(@NonNull Throwable e) {
                                Log.d(TAG," onError "+e.getMessage());
                           }

                           @Override
                           public void onComplete() {
                               Log.d(TAG," onCompleted ");


                           }
                       }
               )
       );

    }

    private void getStarWarsCharList(StarWarsResults results) {

       detailsList = results.getResults();
        if(results.getNext()!= null) {
            next = results.getNext();
        }
        previous = results.getPrevious();


        if(detailsList.size()!= 0) {
            Log.d(TAG," "+detailsList.size());
            List<StarWarsCharacters> list = new ArrayList<>();
            for(int i=0;i<=detailsList.size()-1;i++) {
               list.add(new StarWarsCharacters(detailsList.get(i).getName(),detailsList.subList(i,i+1)));
            }
            adapter = new StarWarsAdapter(list);
           if( progressBar.isShown()){
               progressBar.hide();
           }
            recyclerView.setAdapter(adapter);

        }

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(_disposables!= null && !_disposables.isDisposed()) {
            _disposables.dispose();
        }

    }



}
