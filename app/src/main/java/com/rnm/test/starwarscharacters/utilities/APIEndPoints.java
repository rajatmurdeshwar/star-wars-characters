package com.rnm.test.starwarscharacters.utilities;

import com.rnm.test.starwarscharacters.data.StarWarsPersonsDetails;
import com.rnm.test.starwarscharacters.data.StarWarsResults;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Mahe on 11/20/2017.
 */

public interface APIEndPoints {

    @GET("people/")
    Observable<StarWarsResults> getStarWarsCharacters(@Query("page") int num);

    @GET("people/{i}/")
    Observable<StarWarsPersonsDetails> getStarWarsCharDetails(@Path("i") String i);

    @GET("people/")
    Call<StarWarsResults> getStarWarsChar();

    @GET("people/{i}/")
    Call<StarWarsPersonsDetails> getStarWarsCharDetail(@Path("i") int i);
}
