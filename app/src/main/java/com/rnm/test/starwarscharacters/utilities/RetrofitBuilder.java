package com.rnm.test.starwarscharacters.utilities;

import android.support.v7.app.AppCompatActivity;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mahe on 11/20/2017.
 */

public class RetrofitBuilder {

    //private static Retrofit obj = null;
    public static final String BASE_URL ="https://swapi.co/api/";
    public static Retrofit getRetrofit() {

        return  new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    public APIEndPoints getStarWarsDetails() {
        final Retrofit retrofit = getRetrofit();
        return retrofit.create(APIEndPoints.class);
    }

}
