package com.rnm.test.starwarscharacters;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

/**
 * Created by Mahe on 11/20/2017.
 */

public class DetailsViewHolder extends ChildViewHolder {

    private TextView height, mass, hairColor, skinColor, eyeColor, birthYear, gender;



    public DetailsViewHolder(View itemView) {
        super(itemView);
        height = (TextView) itemView.findViewById(R.id.detail_height_data);
        mass = (TextView)itemView.findViewById(R.id.detail_mass_data);
        hairColor =(TextView) itemView.findViewById(R.id.detail_hair_color_data);
        skinColor =(TextView) itemView.findViewById(R.id.detail_skin_color_data);
        eyeColor = (TextView)itemView.findViewById(R.id.detail_eye_color_data);
        birthYear = (TextView)itemView.findViewById(R.id.detail_birth_year_data);
        gender = (TextView)itemView.findViewById(R.id.detail_gender_data);
    }

    public void setHeight(String height) {
        this.height.setText(height);
    }

    public void setMass(String mass) {
        this.mass.setText(mass);
    }

    public void setHairColor(String hairColor) {
        this.hairColor.setText(hairColor);
    }

    public void setSkinColor(String skinColor) {
        this.skinColor.setText(skinColor);
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor.setText(eyeColor);
    }

    public void setBirthYear(String birthYear) {
        this.birthYear.setText(birthYear);
    }

    public void setGender(String gender) {
        this.gender.setText(gender);
    }



}
