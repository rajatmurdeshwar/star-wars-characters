package com.rnm.test.starwarscharacters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rnm.test.starwarscharacters.data.StarWarsCharacters;
import com.rnm.test.starwarscharacters.data.StarWarsPersonsDetails;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by Mahe on 11/20/2017.
 */

public class StarWarsAdapter extends ExpandableRecyclerViewAdapter<NameViewHolder, DetailsViewHolder> {


    public StarWarsAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public NameViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_name, parent, false);
        return new NameViewHolder(view);
    }

    @Override
    public DetailsViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_details, parent, false);
        return new DetailsViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(DetailsViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {

        Log.d("StarWarsAdapter "," "+childIndex+" "+flatPosition);

      final StarWarsPersonsDetails details = ((StarWarsCharacters) group).getItems().get(childIndex);

           holder.setHeight(details.getHeight());
           holder.setMass(details.getMass());
           holder.setEyeColor(details.getEyeColor());
           holder.setHairColor(details.getHairColor());
           holder.setGender(details.getGender());
           holder.setSkinColor(details.getSkinColor());
           holder.setBirthYear(details.getBirthYear());



    }

    @Override
    public void onBindGroupViewHolder(NameViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setNameHeader(group);

    }
}
