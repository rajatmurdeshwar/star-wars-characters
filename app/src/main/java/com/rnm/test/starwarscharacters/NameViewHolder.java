package com.rnm.test.starwarscharacters;

import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

/**
 * Created by Mahe on 11/20/2017.
 */

public class NameViewHolder extends GroupViewHolder {

    private TextView name;
    private ImageView arrow;




    public NameViewHolder(View itemView) {
        super(itemView);

        name = (TextView) itemView.findViewById(R.id.name_header);
        arrow = (ImageView) itemView.findViewById(R.id.list_item_genre_arrow);
    }

    public void setNameHeader(ExpandableGroup name) {
        this.name.setText(name.getTitle());

    }



    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
        arrow.setAnimation(rotate);
    }
}
